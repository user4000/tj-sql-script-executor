﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using SecurityDriven.Inferno;
using SecurityDriven.Inferno.Kdf;
using SecurityDriven.Inferno.Mac;
using ProjectStandard.Extensions;
using System.Security.Authentication;

namespace ProjectStandard
{
  public class CxSecurityStandard
  {
    public static int IterationsForUserPassword { get; } = 1000; // TODO: Протестировать время выполнения и может быть уменьшить это значение //

    public static int HashOfPasswordResultLengthBytes { get; } = 512;

    public static int IterationsForApiKey { get; } = 100;

    public static int ApiKeyResultLengthBytes { get; } = 32;

    public static int LoginMinimumLength { get; } = 2;

    public static int SaltMinimumLength { get; } = 8;

    public static int PasswordMinimumLength { get; } = 5;

    public static int SaltLength { get; } = 32;

    public static string Delimiter { get; } = "-";

    public static string Empty { get; } = string.Empty;

    private static PBKDF2 Pbkdf2 { get; set; }

    public static CryptoRandom CryptoRandomLocal { get; set; } = new CryptoRandom();

    public const string CharsForApiKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public const string CharsForLogin = "0123456789_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; // TODO: Ввести стандарт = логин может быть только заглавными буквами //

    public const string CharsForSalt = "0123456789_!@#$%^&()+-=[]<>{}|;:,.?~ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public const string CharsForPassword = "0123456789_!@#$%^&()+-=[]<>{}|;:,.?~ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static string HeaderHttpAuthorization { get; } = "Authorization";

    public static string HttpAuthTypeBasic { get; } = "Basic";

    public static string HttpAuthTypeApiKey { get; } = "APIKEY";

    public static string SeparatorLoginPassword { get; } = ":";

    public static string SeparatorSpace { get; } = " ";

    public static bool IsLetter(char c) => (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');

    public static bool IsDigit(char c) => (c >= '0') && (c <= '9');

    public static bool IsSymbol(char c) => (c > 32) && (c < 127) && !IsDigit(c) && !IsLetter(c);

    public static bool ErrorTooShortPassword(string password) => password.Length < PasswordMinimumLength;

    public static bool ErrorTooShortSalt(string salt) => salt.Length < SaltMinimumLength;

    public static bool ErrorTooShortLogin(string login) => login.Length < LoginMinimumLength;

    public static bool ErrorIncorrectPassword(string password) => !IsValidPassword(password);

    public static bool ErrorIncorrectLogin(string login) => !IsValidLogin(login);

    public static bool IsValidValue(string value, string CharSet)
    {
      bool result = true;
      foreach (char c in value)
      {
        if (CharSet.Contains(c) == false) { result = false; break; }
      }
      return result;
    }

    private static bool IsValidPassword(string password) => IsValidValue(password, CharsForPassword);

    private static bool IsValidLogin(string login)
    {
      return IsValidValue(login, CharsForLogin) && (!login.StartsWith("_")) && (!login.EndsWith("_")) && (!login.Contains("__"));
    }

    public static int GetRandom(int A, int B) => CryptoRandomLocal.Next(A, B);

    public static byte[] GetBytes(string s) => Utils.SafeUTF8.GetBytes(s);

    public static string GetString(byte[] array) => Utils.SafeUTF8.GetString(array);

    public static string GenerateSalt()
    {
      return new string(Enumerable.Repeat(CharsForSalt, SaltLength).Select(s => s[CryptoRandomLocal.Next(s.Length)]).ToArray());
    }

    public static string GenerateRandomString(int Length)
    {
      return new string(Enumerable.Repeat(CharsForSalt, Length).Select(s => s[CryptoRandomLocal.Next(s.Length)]).ToArray());
    }

    public static string GenerateRandomId(int Length)
    {
      return new string(Enumerable.Repeat(CharsForApiKey, Length).Select(s => s[CryptoRandomLocal.Next(s.Length)]).ToArray());
    }

    public static string ComputeHash(string Password, string Salt)
    {
      if (Salt.Length < SaltMinimumLength) { return string.Empty; }

      Pbkdf2 = new PBKDF2 // TODO: Если подать на вход строку Salt меньше 8 байтов - то этот метод-конструктор выдает исключение ! //
       (
         HMACFactories.HMACSHA512,
         password: Encoding.ASCII.GetBytes(Password),
         salt: Encoding.ASCII.GetBytes(Salt),
         iterations: IterationsForUserPassword
       );

      byte[] HashSalted = Pbkdf2.GetBytes(HashOfPasswordResultLengthBytes);

      Pbkdf2.Dispose(); Pbkdf2 = null;

      return BitConverter.ToString(HashSalted).Replace(Delimiter, Empty);
    }

    public static AuthenticationHeaderValue CreateBasicAuthenticationHeader(string user, string password)
    {
      return
        new AuthenticationHeaderValue(
            HttpAuthTypeBasic,
            Convert.ToBase64String(
                Encoding.ASCII.GetBytes(user + SeparatorLoginPassword + password)));
    }

    public static ReturnCode DecodeBasicAuthenticationHeader(string HttpHeader)
    {
      string Login = string.Empty, Password = string.Empty;
      
      try
      {
        string base64 = HttpHeader.Right(HttpHeader.Length - HttpAuthTypeBasic.Length).Trim();
        byte[] bytes = Convert.FromBase64String(base64);
        string LoginAndPassword = Encoding.ASCII.GetString(bytes);
        int index = LoginAndPassword.IndexOf(SeparatorLoginPassword);
        Login = LoginAndPassword.Left(index);
        Password = LoginAndPassword.Right(LoginAndPassword.Length - index - 1);
      }
      catch  
      {
        Login = string.Empty; Password = string.Empty;
      }

      ReturnCode code = new ReturnCode(0, 0, Login, Password);

      return code;
    }

    public static AuthenticationHeaderValue CreateApikeyAuthenticationHeader(string apikey) => new AuthenticationHeaderValue(HttpAuthTypeApiKey, apikey);

    public static string DecodeApikeyAuthenticationHeader(string HttpHeader)
    {
      return HttpHeader.Right(HttpHeader.Length - HttpAuthTypeApiKey.Length).Trim();
    }

    public static string GenerateApiKey()
    {
      byte[] array = CryptoRandomLocal.NextBytes(1024);
      string Salt = GenerateSalt();
      if (Salt.Length < SaltMinimumLength) { throw new AuthenticationException($"Ошибка! Не удалось сгенерировать API key. Длина соли меньше {SaltMinimumLength} байтов."); }

      Pbkdf2 = new PBKDF2 // TODO: Если подать на вход строку Salt меньше 8 байтов - то этот метод-конструктор выдает исключение ! //
       (
         HMACFactories.HMACSHA512,
         password: array,
         salt: Encoding.ASCII.GetBytes(Salt),
         iterations: IterationsForApiKey
       );

      byte[] HashSalted = Pbkdf2.GetBytes(ApiKeyResultLengthBytes);

      Pbkdf2.Dispose(); Pbkdf2 = null;

      string Result = string.Empty; int j = 0; int k = CharsForApiKey.Length;

      for (int i = 0; i < HashSalted.Length; i++)
      {
        j = HashSalted[i] % k;
        Result += CharsForApiKey[j];
      }

      return Result;
    }

    public static byte[] Encrypt(byte[] masterKey, ArraySegment<byte> plaintext, ArraySegment<byte>? salt = null)
    { // Does not work in Windows 7 //
      return SuiteB.Encrypt(masterKey, plaintext, salt);
    }

    public static byte[] Decrypt(byte[] masterKey, ArraySegment<byte> ciphertext, ArraySegment<byte>? salt = null)
    { // Does not work in Windows 7 //
      return SuiteB.Decrypt(masterKey, ciphertext, salt);
    }

  }
}
