﻿using System;

namespace ModelStandard
{
  [Serializable]
  public class UniversalCommandArgument
  {
    public string Arg1 { get; set; }
    public string Arg2 { get; set; }
    public string Arg3 { get; set; }
    public string Arg4 { get; set; }
    public string Arg5 { get; set; }

  }
}
