﻿using System;

namespace ModelStandard
{
  [Serializable]
  public class ClassificatorWithEntityName : Classificator
  {
    public string EntityName { get; set; }
  }
}
