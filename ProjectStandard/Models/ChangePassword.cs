﻿using System;

namespace ModelStandard
{
  [Serializable]
  public class ChangePassword
  {
    public string Login { get; set; } = string.Empty;

    public string OldPassword { get; set; } = string.Empty;

    public string NewPassword { get; set; } = string.Empty;
  }
}
