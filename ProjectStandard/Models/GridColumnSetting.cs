﻿using System;

namespace ModelStandard
{
  [Serializable]
  public class GridColumnSetting
  {
    public int Width { get; set; }
    public int Index { get; set; }

  }
}
