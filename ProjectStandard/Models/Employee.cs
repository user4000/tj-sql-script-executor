﻿using System;

namespace ModelStandard
{
  [Serializable]
  public class Employee : DataModel
  {
    public int IdEmployee { get; set; }

    public string SurName { get; set; }

    public string FirstName { get; set; }

    public string Patronymic { get; set; }

    public DateTime DateBirth { get; set; }

    public string RfidCode { get; set; }

    public string Note { get; set; }

    public int IdCategory { get; set; }

    public int IdStatus { get; set; }

    public int IdUnit { get; set; }

    public int IdPosition { get; set; }

    public int IdCountry { get; set; }

    public int IdGender { get; set; }
  }
}
