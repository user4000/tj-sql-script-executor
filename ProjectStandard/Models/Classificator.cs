﻿using System;

namespace ModelStandard
{
  [Serializable]
  public class Classificator : DataAbstractModel
  {
    public int IdObject { get; set; }
    public int? IdParent { get; set; } // NOTE: 2020-07-04 был изменён тип с int на int? //
    public string CodeObject { get; set; }
    public int RankObject { get; set; }
    public string NameShort { get; set; }
    public string NameObject { get; set; }
    public string NoteObject { get; set; }
  }
}
