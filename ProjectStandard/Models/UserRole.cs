﻿using System;
using System.Collections.Generic;

namespace ModelStandard
{
  [Serializable]
  public class UserRole
  {
  
    public int IdRole { get; set; }

    public int ActionSelect { get; set; }

    public string NameObject { get; set; }

  }
}
