﻿using System;

namespace ModelStandard
{
  [Serializable]
  public class Parameter : DataModel
  {
    public int IdTypeParameter { get; set; }
    public int IsEditable { get; set; }
    public int ValueInteger { get; set; }
    public string ValueString { get; set; }
    public string ValueBoolean { get; set; }
    public string DisplayValue { get; set; }
    public string ParameterName { get; set; }
    public string ParameterGroup { get; set; }
    public string ParameterNote { get; set; }
    public string TypeParameterName { get; set; }

  }
}
