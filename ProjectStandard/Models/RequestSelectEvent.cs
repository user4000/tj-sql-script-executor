﻿using System;

namespace ModelStandard
{
  [Serializable]
  public class RequestSelectEvent
  {
    public string Command { get; set; }
    public int IdUser { get; set; }
    public int RowCount { get; set; }
    public int Day { get; set; }
  }
}
