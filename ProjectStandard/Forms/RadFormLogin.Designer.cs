﻿namespace ProjectStandard
{
    partial class RadFormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadFormLogin));
      this.PageViewLogin = new Telerik.WinControls.UI.RadPageView();
      this.PageLogin = new Telerik.WinControls.UI.RadPageViewPage();
      this.PnLogin = new Telerik.WinControls.UI.RadPanel();
      this.LbDisconnect = new Telerik.WinControls.UI.RadLabel();
      this.LbConnect = new Telerik.WinControls.UI.RadLabel();
      this.PicConnection = new System.Windows.Forms.PictureBox();
      this.BtnConnect = new Telerik.WinControls.UI.RadButton();
      this.TxPassword = new Telerik.WinControls.UI.RadTextBoxControl();
      this.TxLogin = new Telerik.WinControls.UI.RadTextBoxControl();
      this.PictureOn = new System.Windows.Forms.PictureBox();
      this.PictureOff = new System.Windows.Forms.PictureBox();
      this.PictureKey = new System.Windows.Forms.PictureBox();
      this.PictureUser = new System.Windows.Forms.PictureBox();
      this.PagePassword = new Telerik.WinControls.UI.RadPageViewPage();
      this.PnChangePassword = new Telerik.WinControls.UI.RadPanel();
      this.PicHint = new System.Windows.Forms.PictureBox();
      this.LxNewPwdAgain = new Telerik.WinControls.UI.RadLabel();
      this.LxNewPwd = new Telerik.WinControls.UI.RadLabel();
      this.LxCurrentPwd = new Telerik.WinControls.UI.RadLabel();
      this.BtnChangePassword = new Telerik.WinControls.UI.RadButton();
      this.TxtNew2 = new Telerik.WinControls.UI.RadTextBoxControl();
      this.TxtNew1 = new Telerik.WinControls.UI.RadTextBoxControl();
      this.TxtOld = new Telerik.WinControls.UI.RadTextBoxControl();
      this.PageHint = new Telerik.WinControls.UI.RadPageViewPage();
      this.LbHint = new Telerik.WinControls.UI.RadLabel();
      ((System.ComponentModel.ISupportInitialize)(this.PageViewLogin)).BeginInit();
      this.PageViewLogin.SuspendLayout();
      this.PageLogin.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnLogin)).BeginInit();
      this.PnLogin.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.LbDisconnect)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.LbConnect)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PicConnection)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BtnConnect)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxPassword)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxLogin)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PictureOn)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PictureOff)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PictureKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PictureUser)).BeginInit();
      this.PagePassword.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnChangePassword)).BeginInit();
      this.PnChangePassword.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PicHint)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.LxNewPwdAgain)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.LxNewPwd)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.LxCurrentPwd)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BtnChangePassword)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxtNew2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxtNew1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxtOld)).BeginInit();
      this.PageHint.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.LbHint)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PageViewLogin
      // 
      this.PageViewLogin.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.PageViewLogin.Controls.Add(this.PageLogin);
      this.PageViewLogin.Controls.Add(this.PagePassword);
      this.PageViewLogin.Controls.Add(this.PageHint);
      this.PageViewLogin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PageViewLogin.ItemSizeMode = ((Telerik.WinControls.UI.PageViewItemSizeMode)((Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth | Telerik.WinControls.UI.PageViewItemSizeMode.EqualHeight)));
      this.PageViewLogin.Location = new System.Drawing.Point(65, 74);
      this.PageViewLogin.Name = "PageViewLogin";
      this.PageViewLogin.SelectedPage = this.PageLogin;
      this.PageViewLogin.Size = new System.Drawing.Size(846, 475);
      this.PageViewLogin.TabIndex = 5;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).ShowItemPinButton = false;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).ItemAlignment = Telerik.WinControls.UI.StripViewItemAlignment.Near;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).ItemFitMode = Telerik.WinControls.UI.StripViewItemFitMode.None;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Top;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).ShowItemCloseButton = false;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).ItemSpacing = 10;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).ItemSizeMode = ((Telerik.WinControls.UI.PageViewItemSizeMode)((Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth | Telerik.WinControls.UI.PageViewItemSizeMode.EqualHeight)));
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).ItemContentOrientation = Telerik.WinControls.UI.PageViewContentOrientation.Auto;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewLogin.GetChildAt(0))).ShowHorizontalLine = false;
      // 
      // PageLogin
      // 
      this.PageLogin.Controls.Add(this.PnLogin);
      this.PageLogin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PageLogin.ItemSize = new System.Drawing.SizeF(147F, 35F);
      this.PageLogin.Location = new System.Drawing.Point(10, 44);
      this.PageLogin.Name = "PageLogin";
      this.PageLogin.Size = new System.Drawing.Size(825, 420);
      this.PageLogin.Text = "Login";
      this.PageLogin.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // PnLogin
      // 
      this.PnLogin.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.PnLogin.Controls.Add(this.LbDisconnect);
      this.PnLogin.Controls.Add(this.LbConnect);
      this.PnLogin.Controls.Add(this.PicConnection);
      this.PnLogin.Controls.Add(this.BtnConnect);
      this.PnLogin.Controls.Add(this.TxPassword);
      this.PnLogin.Controls.Add(this.TxLogin);
      this.PnLogin.Controls.Add(this.PictureOn);
      this.PnLogin.Controls.Add(this.PictureOff);
      this.PnLogin.Controls.Add(this.PictureKey);
      this.PnLogin.Controls.Add(this.PictureUser);
      this.PnLogin.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnLogin.ForeColor = System.Drawing.Color.Black;
      this.PnLogin.Location = new System.Drawing.Point(31, 27);
      this.PnLogin.Name = "PnLogin";
      this.PnLogin.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
      this.PnLogin.Size = new System.Drawing.Size(765, 371);
      this.PnLogin.TabIndex = 1;
      this.PnLogin.Text = "Enter user name and password";
      this.PnLogin.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
      // 
      // LbDisconnect
      // 
      this.LbDisconnect.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LbDisconnect.Location = new System.Drawing.Point(435, 294);
      this.LbDisconnect.Name = "LbDisconnect";
      this.LbDisconnect.Size = new System.Drawing.Size(98, 23);
      this.LbDisconnect.TabIndex = 5;
      this.LbDisconnect.Text = "Disconnect";
      this.LbDisconnect.Visible = false;
      // 
      // LbConnect
      // 
      this.LbConnect.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LbConnect.Location = new System.Drawing.Point(223, 294);
      this.LbConnect.Name = "LbConnect";
      this.LbConnect.Size = new System.Drawing.Size(75, 23);
      this.LbConnect.TabIndex = 5;
      this.LbConnect.Text = "Connect";
      this.LbConnect.Visible = false;
      // 
      // PicConnection
      // 
      this.PicConnection.Location = new System.Drawing.Point(548, 255);
      this.PicConnection.Name = "PicConnection";
      this.PicConnection.Size = new System.Drawing.Size(50, 42);
      this.PicConnection.TabIndex = 4;
      this.PicConnection.TabStop = false;
      // 
      // BtnConnect
      // 
      this.BtnConnect.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BtnConnect.Location = new System.Drawing.Point(223, 253);
      this.BtnConnect.Name = "BtnConnect";
      this.BtnConnect.Size = new System.Drawing.Size(310, 35);
      this.BtnConnect.TabIndex = 1;
      this.BtnConnect.Text = "Connect / Disconnect";
      // 
      // TxPassword
      // 
      this.TxPassword.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxPassword.Location = new System.Drawing.Point(223, 163);
      this.TxPassword.MaxLength = 1000;
      this.TxPassword.Name = "TxPassword";
      this.TxPassword.PasswordChar = '*';
      this.TxPassword.Size = new System.Drawing.Size(310, 35);
      this.TxPassword.TabIndex = 0;
      this.TxPassword.WordWrap = false;
      // 
      // TxLogin
      // 
      this.TxLogin.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxLogin.Location = new System.Drawing.Point(223, 77);
      this.TxLogin.MaxLength = 1000;
      this.TxLogin.Name = "TxLogin";
      this.TxLogin.Size = new System.Drawing.Size(310, 35);
      this.TxLogin.TabIndex = 2;
      this.TxLogin.WordWrap = false;
      // 
      // PictureOn
      // 
      this.PictureOn.Image = ((System.Drawing.Image)(resources.GetObject("PictureOn.Image")));
      this.PictureOn.Location = new System.Drawing.Point(622, 307);
      this.PictureOn.Name = "PictureOn";
      this.PictureOn.Size = new System.Drawing.Size(96, 47);
      this.PictureOn.TabIndex = 2;
      this.PictureOn.TabStop = false;
      this.PictureOn.Visible = false;
      // 
      // PictureOff
      // 
      this.PictureOff.Image = ((System.Drawing.Image)(resources.GetObject("PictureOff.Image")));
      this.PictureOff.Location = new System.Drawing.Point(622, 255);
      this.PictureOff.Name = "PictureOff";
      this.PictureOff.Size = new System.Drawing.Size(96, 46);
      this.PictureOff.TabIndex = 2;
      this.PictureOff.TabStop = false;
      this.PictureOff.Visible = false;
      // 
      // PictureKey
      // 
      this.PictureKey.Image = ((System.Drawing.Image)(resources.GetObject("PictureKey.Image")));
      this.PictureKey.Location = new System.Drawing.Point(539, 163);
      this.PictureKey.Name = "PictureKey";
      this.PictureKey.Size = new System.Drawing.Size(59, 45);
      this.PictureKey.TabIndex = 2;
      this.PictureKey.TabStop = false;
      // 
      // PictureUser
      // 
      this.PictureUser.Image = ((System.Drawing.Image)(resources.GetObject("PictureUser.Image")));
      this.PictureUser.Location = new System.Drawing.Point(539, 77);
      this.PictureUser.Name = "PictureUser";
      this.PictureUser.Size = new System.Drawing.Size(59, 46);
      this.PictureUser.TabIndex = 2;
      this.PictureUser.TabStop = false;
      // 
      // PagePassword
      // 
      this.PagePassword.Controls.Add(this.PnChangePassword);
      this.PagePassword.ItemSize = new System.Drawing.SizeF(147F, 35F);
      this.PagePassword.Location = new System.Drawing.Point(10, 44);
      this.PagePassword.Name = "PagePassword";
      this.PagePassword.Size = new System.Drawing.Size(825, 420);
      this.PagePassword.Text = "Change password";
      this.PagePassword.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // PnChangePassword
      // 
      this.PnChangePassword.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.PnChangePassword.Controls.Add(this.PicHint);
      this.PnChangePassword.Controls.Add(this.LxNewPwdAgain);
      this.PnChangePassword.Controls.Add(this.LxNewPwd);
      this.PnChangePassword.Controls.Add(this.LxCurrentPwd);
      this.PnChangePassword.Controls.Add(this.BtnChangePassword);
      this.PnChangePassword.Controls.Add(this.TxtNew2);
      this.PnChangePassword.Controls.Add(this.TxtNew1);
      this.PnChangePassword.Controls.Add(this.TxtOld);
      this.PnChangePassword.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnChangePassword.ForeColor = System.Drawing.Color.Black;
      this.PnChangePassword.Location = new System.Drawing.Point(31, 27);
      this.PnChangePassword.Name = "PnChangePassword";
      this.PnChangePassword.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
      this.PnChangePassword.Size = new System.Drawing.Size(765, 371);
      this.PnChangePassword.TabIndex = 1;
      this.PnChangePassword.Text = "Type your current password and a new password";
      this.PnChangePassword.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
      // 
      // PicHint
      // 
      this.PicHint.Cursor = System.Windows.Forms.Cursors.Hand;
      this.PicHint.Image = ((System.Drawing.Image)(resources.GetObject("PicHint.Image")));
      this.PicHint.Location = new System.Drawing.Point(47, 299);
      this.PicHint.Name = "PicHint";
      this.PicHint.Size = new System.Drawing.Size(81, 69);
      this.PicHint.TabIndex = 5;
      this.PicHint.TabStop = false;
      // 
      // LxNewPwdAgain
      // 
      this.LxNewPwdAgain.AutoSize = false;
      this.LxNewPwdAgain.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LxNewPwdAgain.Location = new System.Drawing.Point(47, 209);
      this.LxNewPwdAgain.Name = "LxNewPwdAgain";
      this.LxNewPwdAgain.Size = new System.Drawing.Size(262, 35);
      this.LxNewPwdAgain.TabIndex = 4;
      this.LxNewPwdAgain.Text = "Type a new password again";
      // 
      // LxNewPwd
      // 
      this.LxNewPwd.AutoSize = false;
      this.LxNewPwd.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LxNewPwd.Location = new System.Drawing.Point(47, 145);
      this.LxNewPwd.Name = "LxNewPwd";
      this.LxNewPwd.Size = new System.Drawing.Size(262, 35);
      this.LxNewPwd.TabIndex = 4;
      this.LxNewPwd.Text = "New password";
      // 
      // LxCurrentPwd
      // 
      this.LxCurrentPwd.AutoSize = false;
      this.LxCurrentPwd.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LxCurrentPwd.Location = new System.Drawing.Point(47, 78);
      this.LxCurrentPwd.Name = "LxCurrentPwd";
      this.LxCurrentPwd.Size = new System.Drawing.Size(262, 35);
      this.LxCurrentPwd.TabIndex = 4;
      this.LxCurrentPwd.Text = "Current password";
      // 
      // BtnChangePassword
      // 
      this.BtnChangePassword.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BtnChangePassword.Location = new System.Drawing.Point(315, 299);
      this.BtnChangePassword.Name = "BtnChangePassword";
      this.BtnChangePassword.Size = new System.Drawing.Size(310, 35);
      this.BtnChangePassword.TabIndex = 3;
      this.BtnChangePassword.Text = "Change password";
      // 
      // TxtNew2
      // 
      this.TxtNew2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxtNew2.Location = new System.Drawing.Point(315, 209);
      this.TxtNew2.MaxLength = 1000;
      this.TxtNew2.Name = "TxtNew2";
      this.TxtNew2.PasswordChar = '*';
      this.TxtNew2.Size = new System.Drawing.Size(310, 35);
      this.TxtNew2.TabIndex = 2;
      this.TxtNew2.WordWrap = false;
      // 
      // TxtNew1
      // 
      this.TxtNew1.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxtNew1.Location = new System.Drawing.Point(315, 145);
      this.TxtNew1.MaxLength = 1000;
      this.TxtNew1.Name = "TxtNew1";
      this.TxtNew1.PasswordChar = '*';
      this.TxtNew1.Size = new System.Drawing.Size(310, 35);
      this.TxtNew1.TabIndex = 1;
      this.TxtNew1.WordWrap = false;
      // 
      // TxtOld
      // 
      this.TxtOld.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxtOld.Location = new System.Drawing.Point(315, 78);
      this.TxtOld.MaxLength = 1000;
      this.TxtOld.Name = "TxtOld";
      this.TxtOld.PasswordChar = '*';
      this.TxtOld.Size = new System.Drawing.Size(310, 35);
      this.TxtOld.TabIndex = 0;
      this.TxtOld.WordWrap = false;
      // 
      // PageHint
      // 
      this.PageHint.Controls.Add(this.LbHint);
      this.PageHint.ItemSize = new System.Drawing.SizeF(147F, 35F);
      this.PageHint.Location = new System.Drawing.Point(10, 44);
      this.PageHint.Name = "PageHint";
      this.PageHint.Size = new System.Drawing.Size(825, 420);
      this.PageHint.Text = "Hint";
      this.PageHint.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // LbHint
      // 
      this.LbHint.AutoSize = false;
      this.LbHint.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LbHint.Location = new System.Drawing.Point(126, 54);
      this.LbHint.Name = "LbHint";
      this.LbHint.Size = new System.Drawing.Size(577, 313);
      this.LbHint.TabIndex = 0;
      this.LbHint.Text = "<html><p>Инструкция по применению</p><p>1. Выполните пункт 1.</p><p>2. Выполните " +
    "пункт 2.</p><p>3. Выполните пункт 3.</p><p></p><p></p></html>";
      // 
      // RadFormLogin
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(976, 622);
      this.Controls.Add(this.PageViewLogin);
      this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.Name = "RadFormLogin";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "RadFormLogin";
      ((System.ComponentModel.ISupportInitialize)(this.PageViewLogin)).EndInit();
      this.PageViewLogin.ResumeLayout(false);
      this.PageLogin.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnLogin)).EndInit();
      this.PnLogin.ResumeLayout(false);
      this.PnLogin.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.LbDisconnect)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.LbConnect)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PicConnection)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BtnConnect)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxPassword)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxLogin)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PictureOn)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PictureOff)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PictureKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PictureUser)).EndInit();
      this.PagePassword.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnChangePassword)).EndInit();
      this.PnChangePassword.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PicHint)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.LxNewPwdAgain)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.LxNewPwd)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.LxCurrentPwd)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BtnChangePassword)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxtNew2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxtNew1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxtOld)).EndInit();
      this.PageHint.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.LbHint)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPageView PageViewLogin;
    public Telerik.WinControls.UI.RadPageViewPage PageLogin;
    public Telerik.WinControls.UI.RadPanel PnLogin;
    public Telerik.WinControls.UI.RadButton BtnConnect;
    public Telerik.WinControls.UI.RadTextBoxControl TxPassword;
    public Telerik.WinControls.UI.RadTextBoxControl TxLogin;
    public System.Windows.Forms.PictureBox PictureKey;
    public System.Windows.Forms.PictureBox PictureUser;
    public Telerik.WinControls.UI.RadPageViewPage PagePassword;
    public Telerik.WinControls.UI.RadPanel PnChangePassword;
    public System.Windows.Forms.PictureBox PicHint;
    public Telerik.WinControls.UI.RadLabel LxNewPwdAgain;
    public Telerik.WinControls.UI.RadLabel LxNewPwd;
    public Telerik.WinControls.UI.RadLabel LxCurrentPwd;
    public Telerik.WinControls.UI.RadButton BtnChangePassword;
    public Telerik.WinControls.UI.RadTextBoxControl TxtNew2;
    public Telerik.WinControls.UI.RadTextBoxControl TxtNew1;
    public Telerik.WinControls.UI.RadTextBoxControl TxtOld;
    public System.Windows.Forms.PictureBox PicConnection;
    public System.Windows.Forms.PictureBox PictureOn;
    public System.Windows.Forms.PictureBox PictureOff;
    public Telerik.WinControls.UI.RadLabel LbHint;
    public Telerik.WinControls.UI.RadPageViewPage PageHint;
    public Telerik.WinControls.UI.RadLabel LbDisconnect;
    public Telerik.WinControls.UI.RadLabel LbConnect;
  }
}
