﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectStandard
{
  public class ReturnCodeFactory
  {
    public const int ConstReturnCodeError = -1;

    public static readonly int ReturnCodeSuccess = 0;

    public static readonly int ReturnCodeError = ConstReturnCodeError;

    public static readonly int IdObjectError = -1;

    public const string Empty = "";

    public static ReturnCode ErrorCode(string returnMessage = "", string returnNote = "", int returnCode = ConstReturnCodeError)
    {
      return Create(returnCode, IdObjectError, returnMessage, returnNote);
    }

    public static ReturnCode Create(int returnCode, long idObject, string returnMessage, string returnNote)
    {
      return new ReturnCode(returnCode, idObject, returnMessage, returnNote);
    }

    public static ReturnCode SuccessCode(string returnMessage = Empty, string returnNote = Empty, long idObject = 0)
    {
      return new ReturnCode(ReturnCodeSuccess, idObject, returnMessage, returnNote);
    }
    
    public static async Task<ReturnCode> FromHttpResponse(HttpResponseMessage response, bool KeepReturnCodeGivenByStoredProcedure)
    {
      ReturnCode code;
      if (KeepReturnCodeGivenByStoredProcedure)
      {/* В данном варианте метод вернёт именно то значение TTReturnCode которое вернула ХП сервера */
        string json = await response.Content.ReadAsStringAsync();   code = CxConvert.JsonToObject<ReturnCode>(json);
      }
      else
      { /* В данном варианте метод подменяет собой содержимое TTReturnCode которое вернула ХП сервера, возвращая своё собственное значение TTReturnCode на основе HttpResponse Code */
        code = response.IsSuccessStatusCode ? SuccessCode() : ErrorCode(response.ReasonPhrase, response.StatusCode.ToString());
      }
      return code;
    }

    public static async Task<ReturnCode> FromHttpResponse(HttpResponseMessage response)
    {/* В данном варианте метод вернёт именно то значение TTReturnCode которое вернула ХП сервера */   

      if (response.StatusCode == ServerCode.CodeErrorTimeout)
        return ErrorCode("Ошибка! Сервер не ответил на ваш запрос.");

      if (response.StatusCode == ServerCode.CodeError)
        return ErrorCode("Произошла ошибка при выполнении запроса!");

      ReturnCode code;
      try
      {
        string json = await response.Content.ReadAsStringAsync();
        code = CxConvert.JsonToObject<ReturnCode>(json);
      }
      catch (Exception ex)
      {
        code = ErrorCode("Ошибка при попытке получения ответа от сервера! " + ex.Message + " " + ex.StackTrace, ex.Source);
      }

      return code;
    } 
  }
}
