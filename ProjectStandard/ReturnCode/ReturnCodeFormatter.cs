﻿using System.Diagnostics;

namespace ProjectStandard
{
  public class ReturnCodeFormatter
  {
    public static string RemoveSquareBracketsFromStartAndFromEnd(string json) => json.TrimStart('[').TrimEnd(']');

    public static ReturnCode TryToGetReturnCodeFromJson(string json)
    {
      ReturnCode code = null;
      if (MayBeReturnCode(json) == false) return code;
      if (json.StartsWith("[")) json = RemoveSquareBracketsFromStartAndFromEnd(json);

      try
      {
        code = CxConvert.JsonToObject<ReturnCode>(json);       
      }
      catch
      {
        code = null;
      }

      if (code == null)
        try
        {
          code = CxConvert.JsonToObject<ReturnCode>(RemoveSquareBracketsFromStartAndFromEnd(json));
        }
        catch
        {
          code = null;
        }
      //if (code == null) Trace.WriteLine("code=NULL"); else Trace.WriteLine(ToString(code));
      return code;
    }

    public static bool MayBeReturnCode(string json) =>
      json.Contains("\"" + nameof(ReturnCode.Number) + "\"") &&
      json.Contains("\"" + nameof(ReturnCode.Message) + "\"") &&
      json.Contains("\"" + nameof(ReturnCode.IdObject) + "\"") &&
      json.Contains("\"" + nameof(ReturnCode.Note) + "\"");

    public static string ToString(ReturnCode code)
    {
      return $"{code.Number};{code.IdObject};{code.Message};{code.Note}";
    }
  }
}
