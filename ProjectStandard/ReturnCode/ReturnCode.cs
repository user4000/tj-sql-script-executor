﻿using Newtonsoft.Json;
using System;

namespace ProjectStandard
{
  [Serializable]
  public class ReturnCode
  {
    public int Number { get; set; } = -1;

    public long IdObject { get; set; } = -1;

    public string Message { get; set; } = string.Empty;

    public string Note { get; set; } = string.Empty;

    public ReturnCode() {  }

    public ReturnCode(int number, long idObject, string message, string note)
    {
      Number = number; IdObject = idObject; Message = message; Note = note;
    }

    [JsonIgnore]
    public bool Success { get => Number == 0; }

    [JsonIgnore]
    public bool Error { get => Number != 0; }

    [JsonIgnore]
    public int IdObjectAsInt32 { get => CxConvert.ToInt32(IdObject); }

  }
}
