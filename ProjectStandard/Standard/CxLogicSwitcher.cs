﻿namespace ProjectStandard
{
  public static class CxLogicSwitcher
  {

    private const string ConstOn = " V ";

    private const string ConstOff = " X ";

    public static string None { get; } = string.Empty;
    public static string On { get; } = ConstOn;
    public static string Off { get; } = ConstOff;

    public static int GetValue(string LogicSwitcher)
    {
      int result = 0;
      switch (LogicSwitcher)
      {
        case ConstOn: result = 1; break;
        case ConstOff: result = 2; break;
      }
      return result;
    }


    public static string GetStringValue(string LogicSwitcher) => GetValue(LogicSwitcher).ToString();
  }
}
