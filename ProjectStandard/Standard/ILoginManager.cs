﻿using System.Threading.Tasks;

namespace ProjectStandard
{
  public interface ILoginManager : IOutputMessage
  {
    string LastLogin();

    Task<ReturnCode> ChangePassword(string Login, string OldPassword, string NewPassword);

    Task<ReturnCode> Connect(string Login, string Password);

    Task<ReturnCode> Disconnect();

    Task EventConnectButtonClick(ReturnCode code); // В этом методе выводится сообщение если соединение с сервером не удалось //

    void SaveLastLogin(string login, bool SuccessConnection);

    Task<ReturnCode> DeleteApiKey();

    void ClearApiKey();

  }
}