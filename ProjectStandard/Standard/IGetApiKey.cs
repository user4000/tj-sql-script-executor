﻿namespace ProjectStandard
{
  public interface IGetApiKey
  {
    string GetApiKey();
  }
}