﻿using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Net.Http;
using System.IO.Compression;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;

namespace ProjectStandard
{
  public class CxConvert
  {
    public const string Empty = "";

    public static string StringSeparatorStandard { get; } = "|";

    public static char CharSeparatorStandard { get; } = '|';

    public static DateTime DateOrigin { get; } = new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

    public static JsonSerializerSettings jsonIgnoreLoop = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };

    static CxConvert()
    {
      StringSeparatorStandard = CharSeparatorStandard.ToString();
      if (StringSeparatorStandard != CharSeparatorStandard.ToString()) throw new Exception("Ошибка в данных класса TTConvert. StringSeparatorStandard должен быть равен CharSeparatorStandard");
    }

    public static int BytesInMegabyte { get; } = 1048576;

    public static string DateTimeFormat { get; } = "yyyy-MM-dd HH:mm:ss";

    public static string DateOnlyFormat { get; } = "yyyy-MM-dd";

    public static string HttpContentJson { get; } = "application/json";

    public static string Time { get => DateTime.Now.ToString(DateTimeFormat); }

    public static string Date { get => DateTime.Today.ToString(DateOnlyFormat); }

    public static string ToString(DateTime dt) => dt.ToString(DateTimeFormat);

    public static string ToString(object InputObject)
    {
      if (InputObject != null) { return InputObject.ToString(); }
      else { return string.Empty; }
    }

    public static string ToStringDateOnly(DateTime dt) => dt.ToString(DateOnlyFormat);

    public static string StringYyyyMmDd(DateTime dt, string Separator = Empty)
    {
      return $"{(dt.Year)}{Separator}{(dt.Month).ToString("D2")}{Separator}{(dt.Day).ToString("D2")}";
    }

    public static int IntYyyyMmDd(DateTime dt)
    {
      return dt.Year * 10000 + dt.Month * 100 + dt.Day;
    }

    public static string TryConvertToString(object InputObject)
    {
      string result = string.Empty;
      if (InputObject != null)
        try { result = InputObject.ToString(); } catch { }
      return result;
    }

    public static long ToInt64(string value, long Default)
    {
      long f = Default;
      if (long.TryParse(value, out long x)) f = x;
      return f;
    }

    public static long ToInt64(object value, long Default)
    {
      long f = Default;
      if (int.TryParse(value == null ? Default.ToString() : value.ToString(), out int x)) f = x;
      return f;
    }

    public static int ToInt32(string value, int Default)
    {
      int f = Default;
      if (int.TryParse(value, out int x)) f = x;
      return f;
    }

    public static int ToInt32(object value, int Default)
    {
      int f = Default;
      if (int.TryParse(value == null ? Default.ToString() : value.ToString(), out int x)) f = x;
      return f;
    }

    public static int ToInt32(long ValueInt64)
    {
      if (ValueInt64 < int.MinValue) return int.MinValue;
      if (ValueInt64 > int.MaxValue) return int.MaxValue;
      return Convert.ToInt32(ValueInt64);
    }

    public static long BytesToMegabytes(long bytesCount) => bytesCount / BytesInMegabyte;

    public static string ObjectToJson(object value) => JsonConvert.SerializeObject(value);

    public static string ObjectToJsonIgnoreLoop(object value)
    {
      return JsonConvert.SerializeObject(value, Formatting.Indented, jsonIgnoreLoop); 
    }

    public static StringContent ObjectToJsonStringContent(object value) => new StringContent(ObjectToJson(value), Encoding.UTF8, HttpContentJson);

    public static StringContent CreateStringContent(string json) => new StringContent(json, Encoding.UTF8, HttpContentJson);

    public static T JsonToObject<T>(string json) => JsonConvert.DeserializeObject<T>(json);

    public static T ReturnCodeToObject<T>(ReturnCode code) => JsonToObject<T>(code.Message);

    public static T ByteArrayFirstToJsonThenToObject<T>(byte[] array) => JsonToObject<T>(ByteArrayToString(array));

    public static byte[] FileToByteArray(string FileName) => File.ReadAllBytes(FileName);

    public static string ByteArrayToBase64(byte[] array) => Convert.ToBase64String(array);

    public static Image GetImageFromBase64(string base64)
    {
      return Image.FromStream(new MemoryStream(Convert.FromBase64String(base64)));
    }

    public static byte[] ObjectToByteArray(object MyObject)
    {
      if (MyObject == null)
        return null;
      BinaryFormatter bf = new BinaryFormatter();
      using (MemoryStream ms = new MemoryStream())
      {
        bf.Serialize(ms, MyObject);
        return ms.ToArray();
      }
    }

    public static object ByteArrayToObject(byte[] array)
    {
      MemoryStream memStream = new MemoryStream();
      BinaryFormatter binForm = new BinaryFormatter();
      memStream.Write(array, 0, array.Length);
      memStream.Seek(0, SeekOrigin.Begin);
      Object obj = (Object)binForm.Deserialize(memStream);
      return obj;
    }

    public static string ByteArrayToString(byte[] array) => System.Text.Encoding.UTF8.GetString(array, 0, array.Length);

    public static ByteArrayContent ObjectToByteArrayContent(object MyObject)
    {
      if (MyObject == null) return null;
      byte[] data = ObjectToByteArray(MyObject);
      ByteArrayContent byteContent = new ByteArrayContent(data);
      return byteContent;
    }

    public static async Task<string> ReadContentAsString(HttpResponseMessage message)
    {
      Stream receiveStream = await message.Content.ReadAsStreamAsync();
      StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
      return readStream.ReadToEnd();
    }

    public static bool CheckStatus(string StatusValueAsNumber, ModelStandard.EnumStatus Status) => StatusValueAsNumber.Trim() == ((int)Status).ToString();

    public static int ValueInRange(int Value, int RangeMin, int RangeMax) => Math.Max(RangeMin, Math.Min(RangeMax, Value));

    public static byte[] ZipStringToArray(string str)
    {
      var bytes = Encoding.UTF8.GetBytes(str);
      using (var msi = new MemoryStream(bytes))
      using (var mso = new MemoryStream())
      {
        using (var gs = new GZipStream(mso, CompressionMode.Compress)) { msi.CopyTo(gs); }
        return mso.ToArray();
      }
    }

    public static string UnzipArrayFromString(byte[] bytes)
    {
      using (var msi = new MemoryStream(bytes))
      using (var mso = new MemoryStream())
      {
        using (var gs = new GZipStream(msi, CompressionMode.Decompress)) { gs.CopyTo(mso); }
        return Encoding.UTF8.GetString(mso.ToArray());
      }
    }

    public static byte[] CompressArray(byte[] data)
    {
      using (var compressedStream = new MemoryStream())
      using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
      {
        zipStream.Write(data, 0, data.Length);
        zipStream.Close();
        return compressedStream.ToArray();
      }
    }

    public static byte[] DecompressArray(byte[] data)
    {
      using (var compressedStream = new MemoryStream(data))
      using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
      using (var resultStream = new MemoryStream())
      {
        zipStream.CopyTo(resultStream);
        return resultStream.ToArray();
      }
    }

    public static byte[] ObjectToCompressedByteArray(object MyObject) => CompressArray(ObjectToByteArray(MyObject));

    public static object CompressedByteArrayToObject(byte[] array) => ByteArrayToObject(DecompressArray(array));

    public static byte[] ObjectToByteArray(object MyObject, bool Compress)
    {
      if (Compress)
      {
        return ObjectToCompressedByteArray(MyObject);
      }
      else
      {
        return ObjectToByteArray(MyObject);
      }
    }

    public static object ByteArrayToObject(byte[] array, bool Decompress)
    {
      if (Decompress)
      {
        return CompressedByteArrayToObject(array);
      }
      else
      {
        return ByteArrayToObject(array);
      }
    }

    private static object DeserializeFromStream(Stream stream)
    {
      IFormatter formatter = new BinaryFormatter();
      stream.Seek(0, SeekOrigin.Begin);
      object o = formatter.Deserialize(stream);
      return o;
      //catch (Exception ex) { Console.WriteLine("DeserializeFromStream");Console.WriteLine(ex.Source); Console.WriteLine(ex.Message); Console.WriteLine(ex.StackTrace); return null; }
    }

    public static string StreamToJson(Stream stream)
    {
      StreamReader reader = new StreamReader(stream); string ResponseBody = reader.ReadToEnd(); reader.Close(); return ResponseBody;
    }

    public static async Task<string> StreamToJsonAsync(Stream stream)
    {
      StreamReader reader = new StreamReader(stream);
      string ResponseBody = await reader.ReadToEndAsync();
      reader.Close();
      return ResponseBody;
    }

    public static ModelStandard.SimpleEntity StringToSimpleEntity(string item)
    {
      if (item.Contains(StringSeparatorStandard))
      {
        string[] array = item.Split(CharSeparatorStandard);
        return new ModelStandard.SimpleEntity()
        {
          IdObject = ToInt32(array[0], 0),
          NameObject = array[1]
        };
      }
      else return null;
    }

    public static List<ModelStandard.SimpleEntity> StringListToSimpleEntityList(List<string> list)
    {
      List<ModelStandard.SimpleEntity> result = new List<ModelStandard.SimpleEntity>();
      ModelStandard.SimpleEntity entity;
      foreach (string item in list)
      {
        entity = StringToSimpleEntity(item);
        if (entity != null) result.Add(entity);
      }
      return result;
    }

    public static BindingList<ModelStandard.SimpleEntity> StringListToSimpleEntityBindingList(List<string> list)
    {
      BindingList<ModelStandard.SimpleEntity> result = new BindingList<ModelStandard.SimpleEntity>();
      ModelStandard.SimpleEntity entity;
      foreach (string item in list)
      {
        entity = StringToSimpleEntity(item);
        if (entity != null) result.Add(entity);
      }
      return result;
    }

    public static string DecimalToBase36(long decimalNumber) => DecimalToArbitrarySystem(decimalNumber, 36);

    public static string DecimalToArbitrarySystem(long decimalNumber, int radix = 36)
    {
      const int BitsInLong = 64;
      const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

      if (radix < 2 || radix > Digits.Length)
        throw new ArgumentException("The radix must be >= 2 and <= " +
            Digits.Length.ToString());

      if (decimalNumber == 0)
        return "0";

      int index = BitsInLong - 1;
      long currentNumber = Math.Abs(decimalNumber);
      char[] charArray = new char[BitsInLong];

      while (currentNumber != 0)
      {
        int remainder = (int)(currentNumber % radix);
        charArray[index--] = Digits[remainder];
        currentNumber = currentNumber / radix;
      }

      string result = new String(charArray, index + 1, BitsInLong - index - 1);
      if (decimalNumber < 0)
      {
        result = "-" + result;
      }

      return result;
    }

    public static DateTime ConvertFromUnixTimestamp(double timestamp)
    {
      return DateOrigin.AddSeconds(timestamp);
    }

    public static double ConvertToUnixTimestamp(DateTime date)
    {
      TimeSpan diff = date.ToUniversalTime() - DateOrigin;
      return Math.Floor(diff.TotalSeconds);
    }

    public static string GetSeconds() => ConvertToUnixTimestamp(DateTime.Now).ToString();

    public static string GetSecondsBase36()
    {
      string s = ConvertToUnixTimestamp(DateTime.Now).ToString();
      if (Int64.TryParse(s, out long x))
        return DecimalToBase36(x);
      else
        return s;
    }
  }
}


