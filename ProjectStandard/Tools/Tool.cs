﻿using System;
using System.Diagnostics;
using System.IO;

namespace ProjectStandard
{
  public class Tool
  {
    public static bool IsEmpty(string value) => string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value.Trim());

    public static string GetDateOfPdbFile()
    {
      return CxConvert.ToString
        (
        File.GetLastWriteTime($"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.pdb")
        );
    }

    public static string GetDateOfFile(string FileName)
    {
      return CxConvert.ToString(File.GetLastWriteTime(FileName));
    }

    public static string GetVersionOfDllFile(string FileName)
    {
      FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(FileName);
      return myFileVersionInfo.FileVersion;
    }

    public static void Debug(string value) => Console.WriteLine(value); // TODO: В релизе нужно убрать все вызовы этого метода

    public static string GetExecutingAssemblyPath()=> Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
   

  }
}
