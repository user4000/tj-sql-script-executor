﻿namespace ProjectStandard
{
  public class JsonRequestFactory
  {
    public IGetApiKey ApikeyProvider { get; } = null;

    private JsonRequestFactory(IGetApiKey apikeyProvider)
    {
      ApikeyProvider = apikeyProvider;
    }

    public T Create<T>() where T : JsonRequestBase, new()
    {
      T request = new T();
      //request.ApiKey = ApikeyProvider.GetApiKey(); Раньше, до 2020-07-03, был стандарт: ApiKey передавался не только в заголовке http-запроса но и в самом POCO-объекте //
      return request;
    }

    public static JsonRequestFactory Init(IGetApiKey apikeyProvider) => new JsonRequestFactory(apikeyProvider);
  }
}
