﻿using System;

namespace ProjectStandard
{
  [Serializable]
  public abstract class JsonRequestBase : ModelStandard.DataAbstractModel
  {
    public virtual string TypeRequest { get; set; } = string.Empty;

    public JsonRequestBase() => TypeRequest = this.GetType().Name; // Constructor //
  
  }
}
