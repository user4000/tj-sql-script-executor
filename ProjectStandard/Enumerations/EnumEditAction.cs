﻿namespace ModelStandard
{
  public enum EnumEditAction
  {
    AddNewItem,
    DeleteItem,
    UpdateIdObject,
    UpdateNameObject
  }
}
