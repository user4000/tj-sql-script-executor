﻿namespace ModelStandard
{
  public enum EnumEntitiesReferencedByUser
  {
    Status,
    Unit,
    Position,
    Role,
    Entity,
    RoleEditable,
    Employee_Category
  }
}
