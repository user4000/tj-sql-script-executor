﻿namespace ModelStandard
{
  public enum TypeParameter : int
  {
    Unknown = 0,
    Integer = 1,
    String = 2,
    Boolean = 3
  }
}
