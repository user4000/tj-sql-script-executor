﻿using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace ProjectStandard
{
  public static class XxRadForm
  {
    public static Font MyFont { get; } = new Font("Verdana", 9);

    public static void ZzSetMessageBoxFontVersion2(this RadForm form) // NOTE: Client application should call this method only 1 time //
    {
      RadMessageBox.Instance.FormElement.TitleBar.Font = MyFont;
      foreach (Control ctrl in RadMessageBox.Instance.Controls)
      {
        if (ctrl is RadLabel) //change the main text font
        {
          ctrl.Font = MyFont;
        }
        if (ctrl is RadButton) // change the button font
        {
          ctrl.Font = MyFont;
        }
      }
    }
  }
}

