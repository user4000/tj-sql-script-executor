﻿using System;
using System.Linq;
using System.Collections.Generic;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using System.Windows.Forms;

namespace ProjectStandard
{
  public static class XXRadGridView
  {
    private static string ZZGetStringValue(this RadGridView grid, string GridColumnName)
    {// Получим значение ячейки в указанном столбце текущей строки //
      string s = string.Empty;
      try
      {       
        GridViewRowInfo row = grid.CurrentRow ?? grid.SelectedRows[0];
        if (row == null) return s;
        s = Convert.ToString(row.Cells[GridColumnName].Value); /* Внимание! Имя столбца грида а не имя столбца данных таблицы */
      }
      catch { }
      return s;
    }

    public static string ZZGetStringValueByFieldName(this RadGridView grid, string FieldName) => ZZGetStringValue(grid, CxStandard.GetGridColumnName(FieldName));

    public static int ZZGetInt32ValueByFieldName(this RadGridView grid, string FieldName, int DefaultValue) => CxConvert.ToInt32(ZZGetStringValue(grid, CxStandard.GetGridColumnName(FieldName)), DefaultValue);

    public static long ZZGetInt64ValueByFieldName(this RadGridView grid, string FieldName, long DefaultValue) => CxConvert.ToInt64(ZZGetStringValue(grid, CxStandard.GetGridColumnName(FieldName)), DefaultValue);

    public static void ZZSetGridMainProperties(this RadGridView grid, int RowHeight = 25, bool AllowRowResize = false)
    {
      grid.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.None;
      grid.TableElement.RowHeight = RowHeight;
      grid.AllowRowResize = AllowRowResize;
    }

    public static void ZZLoadColumnWidth(this RadGridView grid, Dictionary<string, int> d)
    {
      int width = 0;
      try
      {
        for (int i = 0; i < grid.ColumnCount; i++)
          if (d.TryGetValue(grid.Columns[i].FieldName, out width))
            grid.Columns[i].Width = width;
      }
      catch { }
    }

    public static void ZZSaveColumnWidth(this RadGridView grid, Dictionary<string, int> d)
    {
      d.Clear();
      for (int i = 0; i < grid.ColumnCount; i++)
        d.Add(grid.Columns[i].FieldName, grid.Columns[i].Width);
    }

    public static GridViewDataColumn ZZGetByFieldName(this RadGridView grid, string FieldName)
    {
      GridViewDataColumn[] collection = grid.Columns.GetColumnByFieldName(FieldName);
      if (collection.Count<GridViewDataColumn>() == 1) return collection.First(); else return null;
    }

    public static int ZZGetColumnWidthByFieldName(this RadGridView grid, string FieldName)
    {
      GridViewDataColumn[] collection = grid.Columns.GetColumnByFieldName(FieldName);
      if (collection.Count<GridViewDataColumn>() == 1) return collection.First().Width; else return -1;
    }

    public static bool ZZIsColumnVisibleByFieldName(this RadGridView grid, string FieldName)
    {
      GridViewDataColumn[] collection = grid.Columns.GetColumnByFieldName(FieldName);
      if (collection.Count() == 1) return collection.First().IsVisible; else return false;
    }

    public static int ZZGetRowIndex(this RadGridView grid, string GridColumnName, string Value, bool MakeCurrent)
    {
      int rowIndex = -1; GridViewRowInfo row = null;

      try
      {
        row = grid.Rows.Cast<GridViewRowInfo>().Where(r => r.Cells[GridColumnName].Value.ToString()==Value).First();
        rowIndex = row.Index;
      }
      catch { rowIndex = -1; }

      if ((rowIndex >= 0) && MakeCurrent) grid.Rows[rowIndex].IsCurrent = true;

      return rowIndex;
    }

    public static void ZzSelectRow(this RadGridView grid, string FieldName, string Value)
    {     
      try
      {
        GridViewRowInfo row = grid.Rows.Cast<GridViewRowInfo>().Where(r => r.Cells[CxStandard.GetGridColumnName(FieldName)].Value.ToString() == Value).First();
        row.IsCurrent = true;
        row.IsSelected = true;
      }
      catch
      {

      }
    }

    public static int ZZGetRowIndexForeach(this RadGridView grid, string GridColumnName, string Value, bool MakeCurrent)
    {
      int rowIndex = -1;
      foreach (GridViewRowInfo row in grid.Rows)
      {
        if (row.Cells[GridColumnName].Value.ToString().Equals(Value))
        {
          rowIndex = row.Index;
          break;
        }
      }
      if ((rowIndex >= 0) && MakeCurrent) grid.Rows[rowIndex].IsCurrent = true;
      return rowIndex;
    }

    public static void ZZSetAllColumnsReadOnly(this RadGridView grid)
    {
      foreach (GridViewDataColumn col in grid.Columns)
        if (!col.ReadOnly) col.ReadOnly = true;
    }

    public static void ZZDisposeAllColumns(this RadGridView grid)
    {
      GridViewDataColumn col;
      for (int i = 0; i < grid.ColumnCount; i++)
      {
        col = grid.Columns[0];
        grid.Columns.Remove(col);
        col.FieldName = string.Empty;
        col.Dispose();
      }
      grid.Columns.Clear();
    }

    public static void ZZDispose(this RadGridView grid)
    {
      grid.Visible = false;
      grid.DataSource = null;
      grid.ZZDisposeAllColumns();
      grid.Dispose();
    }

    public static void ZzFilter(this RadGridView grid, string FieldName, FilterOperator filterOperator, object value, bool ClearPreviousFilter = true)
    {
      if (ClearPreviousFilter) grid.FilterDescriptors.Clear();
      grid.FilterDescriptors.Add(CxStandard.GetGridColumnName(FieldName), FilterOperator.StartsWith, value);
    }

    public static void ZzGridClearSelection(this RadGridView grid, bool ClearFilter)
    {
      if (grid == null) return;
      if (ClearFilter) grid.FilterDescriptors.Clear();
      grid.GridNavigator.ClearSelection();
      grid.CurrentRow = null;
    }

    public static RadForm ZzGetParentForm(this RadGridView grid)
    {
      Control parent = grid.Parent;
      while (!(parent is RadForm))
      {
        parent = parent.Parent;
        if (parent == null) return null;
        if (parent is RadForm) return parent as RadForm;
      }
      return null;
    }


    public static void ZzScrollToCurrentRow(this RadGridView grid)
    {
      GridTableElement tableElement = grid.CurrentView as GridTableElement;
      GridViewRowInfo row = grid.CurrentRow;
      if (tableElement != null && row != null)
      {
        tableElement.ScrollToRow(row);
      }
    }
  }
}

/*


public static async Task ZZFilterRefresh(this RadGridView grid)
{
  grid.EnableFiltering = false;
  var t = Task.Factory.StartNew(() => { Task.Delay(10).Wait(); }); await t;
  grid.EnableFiltering = true;
}


*/

