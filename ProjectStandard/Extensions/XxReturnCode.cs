﻿namespace ProjectStandard
{
  public static class XxReturnCode
  {
    public static string ZzToString(this ReturnCode code) => ReturnCodeFormatter.ToString(code);

  }
}