﻿using System;
using Telerik.WinControls.UI;

namespace ProjectStandard
{
  public static class XXGridViewDataColumn
  {
    public static string ZzGuid(this GridViewDataColumn column) => column.Name + "-" + Guid.NewGuid().ToString();

    public static void ZzAdd(this GridViewDataColumn column, RadGridView grid) => grid.MasterTemplate.Columns.Add(column);

    public static GridViewDataColumn ZzPin(this GridViewDataColumn column)
    {
      column.IsPinned = true; return column;      
    }

    public static GridViewDataColumn ZzDateOnly(this GridViewDataColumn column)
    {
      if (column.DataType != typeof(DateTime)) column.DataType = typeof(DateTime);
      column.FormatString = "{0: yyyy-MM-dd}";
      return column;
    }

    public static GridViewDataColumn ZzHoursAndMinutes(this GridViewDataColumn column)
    {
      if (column.DataType != typeof(DateTime)) column.DataType = typeof(DateTime);
      column.FormatString = "{0: HH:mm}";
      return column;
    }

    public static GridViewDataColumn ZzHide(this GridViewDataColumn column)
    {
      column.Width = 0; column.IsVisible = false; return column;
    }

    public static GridViewDataColumn ZzShow(this GridViewDataColumn column, int width)
    {
      column.Width = width; column.IsVisible = true; return column;
    }
  }
}

