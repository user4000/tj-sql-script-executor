﻿using System;
using System.Linq;

namespace ProjectStandard.Extensions
{
  public static class XXString
  {
    public static string Left(this string value, int length)
    {
      if (string.IsNullOrEmpty(value)) return value;

      length = Math.Abs(length);

      return (value.Length <= length
             ? value
             : value.Substring(0, length)
             );
    }

    public static string Right(this string value, int length)
    {
      value = (value ?? string.Empty);

      length = Math.Abs(length);

      return (value.Length >= length)
          ? value.Substring(value.Length - length, length)
          : value;
    }

    public static string SurroundWithDoubleQuotes(this string text)
    {
      return SurroundWith(text, "\"");
    }

    public static string SurroundWith(this string text, string ends)
    {
      return ends + text + ends;
    }

    public static string FirstCharToUpper(this string input)
    {
      switch (input)
      {
        case null: return string.Empty; // throw new ArgumentNullException(nameof(input));
        case "": return string.Empty;   // throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
        default: return input.First().ToString().ToUpper() + input.Substring(1);
      }
    }

    public static string ZzTrimStart(this string target, string trimString)
    {
      if (string.IsNullOrEmpty(trimString)) return target;

      string result = target;
      while (result.StartsWith(trimString))
      {
        result = result.Substring(trimString.Length);
      }

      return result;
    }

    public static string ZzTrimEnd(this string target, string trimString)
    {
      if (string.IsNullOrEmpty(trimString)) return target;

      string result = target;
      while (result.EndsWith(trimString))
      {
        result = result.Substring(0, result.Length - trimString.Length);
      }

      return result;
    }
  }
}
