﻿using Telerik.WinControls.UI;

namespace ProjectStandard
{
  public static class XXGridViewRowInfo
  {
    public static string ZzGetCellValue(this GridViewRowInfo row, string FieldName) => row.Cells[CxStandard.GetGridColumnName(FieldName)].Value.ToString();
  }
}

