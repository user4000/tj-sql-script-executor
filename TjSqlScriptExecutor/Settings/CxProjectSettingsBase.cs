﻿using System;
using System.Drawing;
using System.ComponentModel;
using Telerik.WinControls.UI;
using System.Collections.Generic;
using TJFramework;
using ModelStandard;
using ProjectStandard;
using TJFramework.ApplicationSettings;
using static TjSqlScriptExecutor.Program;

namespace TjSqlScriptExecutor
{
  public class CxProjectSettingsBase : TJStandardApplicationSettings
  {
    [Category("Connection")]
    [DisplayName("MS SQL Server name")]
    [RadSortOrder(0)]
    public virtual string Server { get; set; } = string.Empty;

    [Category("Connection")]
    [DisplayName("Database name")]
    [RadSortOrder(1)]
    public virtual string Database { get; set; } = string.Empty;


    [Category("Connection")]
    [DisplayName("User name")]
    [RadSortOrder(2)]
    public virtual string Login { get; set; } = string.Empty;

    [Category("Login")]
    [DisplayName("Last login time")]
    public virtual string TimeOfLastSuccessLogin { get { return GetDateTime(HiddenTimeOfLastSuccessLogin); } }

    [Category("Appearance ")]
    [DisplayName("Tab layout")]
    public virtual StripViewAlignment MainPageOrientation { get; set; } = StripViewAlignment.Top;

    [Category("Application")]
    [DisplayName("Encoding for outputting OS command results")]
    public virtual string CommandEncoding { get; set; } = "CP866";

    [Browsable(false)]
    public virtual DateTime HiddenTimeOfLastSuccessLogin { get; set; } = DateTime.Today.AddYears(-100);

    [Browsable(false)]
    public override string TextFileUserSettings { get; }

    [Browsable(false)]
    public override  string FolderSettings { get; }

    public override void EventAfterSaving() { }
    public override void EventBeforeSaving() { }
    public override void PropertyValueChanged(string property_name)
    {
      //-----------------------------------------------------------------------------------------------------------------------------------
      if (property_name == nameof(this.FolderSettings))
      {

      }
      //-----------------------------------------------------------------------------------------------------------------------------------
      if (property_name == nameof(MainPageOrientation))
        TJFrameworkManager.Service.SetMainPageViewOrientation(MainPageOrientation);
      //-----------------------------------------------------------------------------------------------------------------------------------
    }
  }
}


/*

  EXAMPLE OF ATTRIBUTES

  [Serializable]
  public class MySettings : TJStandardUserSettings
  {
    [Category("Category 1")]
    public string MyString1 { get; set; } = "Privet 1111";

    [Category("Category 1")]
    public DateTime MyDatetime1 { get; set; } = DateTime.Now;

    [Category("Category 2")]
    public Font MyFont1 { get; set; } = new Font("Verdana", 14F, FontStyle.Italic);

    [Category("Category 2")]
    public Color MyColor1 { get; set; } = Color.LightGreen;

    [Category("File Location Example")]
    [Editor(typeof(PropertyGridBrowseEditor), typeof(BaseInputEditor))] // File name dialog //
    public string FileLocation1 { get; set; }

    [Category("Range Example")]
    [RadRange(1, 5)]
    public byte DoorsCount { get; set; } = 4;


    [Browsable(false)]
    public int MyHiddenProperty { get; set; } = 890110000;

    [Category("Read Only Example")]
    [ReadOnly(true)]
    public int Count { get; set; } = 18991;

    [Category("Visible Name differs from Property Name")]
    [DisplayName("This is visible name of a property!")]
    public string PropertyName { get; set; }

    [Category("Description of property example")]
    [Description("The manufacturer of the item")]
    public string Manufacturer { get; set; }

    [Category("Login")]
    [RadSortOrder(0)]
    public string ServerName { get; set; } = "MyServer";

    [Category("Login")]
    [RadSortOrder(1)]
    [Description("User account")]
    public string Username { get; set; } = "Vasya";

    [Category("Login")]
    [RadSortOrder(2)]
    [Description("User password")]
    [PasswordPropertyText(true)]
    public string Password { get; set; } = "";

    [Category("Login"), RadSortOrder(3)]
    public bool Connect { get; set; } = false;

    [Category("Login")]
    [RadSortOrder(4)]
    [ReadOnly(true)]
    [DisplayName("Connection state")]
    public bool ConnectionState { get; set; }


    public DateTime inner_Date_Time = TJStandardDateTimeDefaultValue;

    [Category("Login")]
    [RadSortOrder(5)]
    public string MyDateTime
    {
      get { return GetDateTime(inner_Date_Time); }
      set { inner_Date_Time = SetDateTime(value, inner_Date_Time); }
    }

    [Category("Login")]
    [RadSortOrder(6)]
    public MyEnum my_enum { get; set; } = MyEnum.e_Three;


    public override void PropertyValueChanged(string property_name)
    {
      //Ms.Message(MsgType.Info, property_name, "Changed!", 4,MessagePosition.pos_SC); 
    }


    public override void EventBeforeSaving()
    {
      Password = "";
      //MessageBox.Show("EventBefore_Saving");
    }

    public override void EventAfterSaving()
    {
      Password = "12345";
      //MessageBox.Show("EventAfter_Saving");
    }
  }

*/


