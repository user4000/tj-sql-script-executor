﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;
using System.Security.Permissions;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJFramework;
using TJFramework.FrameworkSettings;
using ModelStandard;
using ProjectStandard;
using static TJFramework.Logger.Manager;
using static TJFramework.ApplicationSettings.TJStandardApplicationSettings;

namespace TjSqlScriptExecutor
{
  internal static class Program
  {
    internal static CxManager Manager { get; private set; } = null; // CxManagerFactory.Create();
    internal static CxProjectSettings USettings { get => TJFrameworkManager.ApplicationSettings<CxProjectSettings>(); }
    internal static TJStandardFrameworkSettings FSettings { get; } = TJFrameworkManager.FrameworkSettings;

    internal static string GetRandomId() => CxSecurityStandard.GenerateRandomId(20);

    internal static string Margin(byte spaces) => new string(' ', spaces);

    internal static string AssemblyDirectory
    {
      get
      {
        string codeBase = Assembly.GetExecutingAssembly().CodeBase;
        UriBuilder uri = new UriBuilder(codeBase);
        string path = Uri.UnescapeDataString(uri.Path);
        return Path.GetDirectoryName(path);
      }
    }

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
    static void Main()
    {
      SetExceptionHandler(); // Создание всех объектов-членов класса Program должно быть строго после запуска этого метода, а не до запуска //

      Manager = CxManagerFactory.Create();

      System.Diagnostics.Debug.IndentSize = 1;
      System.Diagnostics.Debug.IndentLevel = 1;

      TJFrameworkManager.Logger.FileSizeLimitBytes = 1000000;
      TJFrameworkManager.Logger.Create(Assembly.GetExecutingAssembly().GetName().Name);

      TJFrameworkManager.Service.CreateApplicationSettings<CxProjectSettings>();

      TJFrameworkManager.Service.SetMainFormCaption("TJ Script executor for MS SQL Server");
      TJFrameworkManager.Service.AddForm<RadFormLogin>("Login", true, true);
      TJFrameworkManager.Service.AddForm<FxSender>("Send a script", true, true);
      TJFrameworkManager.Service.StartPage<RadFormLogin>();

      //FSettings.ConfirmExitButtonText = Margin(3) + Lang.Text("bt_exit_confirm", "Подтвердите выход из программы") + Margin(3);
      FSettings.HeaderFormSettings = "Settings";
      FSettings.HeaderFormLog = "Messages";
      FSettings.HeaderFormExit = "Exit";

      FSettings.PageViewFont = new Font("Verdana", 9);
      FSettings.ValueColumnWidthPercent = 60;
      FSettings.MainPageViewReducePadding = true;
      FSettings.PageViewItemSize = new Size(200, 20);
      FSettings.RememberMainFormLocation = true;
      FSettings.VisualEffectOnStart = true;
      FSettings.VisualEffectOnExit = true;

      Action ExampleOfVisualSettings = () =>
      {
        FSettings.PageViewFont = new Font("Verdana", 9); // <-- customize font of Main Page View //
        FSettings.MainFormMargin = 50;
        FSettings.PageViewItemSize = new Size(200, 20);
        FSettings.ItemSizeMode = PageViewItemSizeMode.EqualSize;
        FSettings.PageViewItemSpacing = 50;
        FSettings.MaxAlertCount = 4;
        FSettings.SecondsAlertAutoClose = 3;
        FSettings.RememberMainFormLocation = true;
      };
      //ExampleOfVisualSettings.Invoke();

      DummyMethodForLoadingSecurityInfernoAssembly();

      TJFrameworkManager.Service.EventBeforeAnyFormStartHandlerLaunched = Manager.EventBeforeAnyFormStart;
      TJFrameworkManager.Service.EventAfterAllFormsAreCreated = async () => await Manager.EventStartWork();
      TJFrameworkManager.Service.EventBeforeMainFormClose = async () => await Manager.EventEndWork();
      TJFrameworkManager.Service.EventPageChanged = Manager.EventSelectedPageChanged;

      TJFrameworkManager.Run();
    }

    internal static void DummyMethodForLoadingSecurityInfernoAssembly()
    {
      string hash = CxSecurityStandard.ComputeHash("MyPassword", "12345678");
    }

    internal static void Debug(string message, bool EnterNewBlock = true)
    {
      //if (EnterNewBlock) System.Diagnostics.Debug.Indent(); else System.Diagnostics.Debug.Unindent();
      System.Diagnostics.Debug.WriteLine(message);
    }

    private static void SetExceptionHandler()
    {
      //==========================================================================================================================
      // Add the event handler for handling UI thread exceptions to the event.
      Application.ThreadException += new ThreadExceptionEventHandler(UIThreadException);

      // Set the unhandled exception mode to force all Windows Forms errors to go through
      // our handler.
      Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

      // Add the event handler for handling non-UI thread exceptions to the event. 
      AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomainUnhandledException);
      //==========================================================================================================================
    }

    // Creates the error message and displays it.
    private static DialogResult ShowThreadExceptionDialog(string title, Exception ex)
    {
      string errorMsg = "An application error occurred. Please contact the adminstrator " +
          "with the following information:\n\n";
      errorMsg = errorMsg + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace;

      return RadMessageBox.Show(errorMsg, title, MessageBoxButtons.AbortRetryIgnore, RadMessageIcon.Error);
      /*
      return MessageBox.Show(errorMsg, title, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);*/
    }

    private static void UIThreadException(object sender, ThreadExceptionEventArgs t)
    {
      DialogResult result = DialogResult.Cancel;
      try
      {
        result = ShowThreadExceptionDialog("An error has been occured", t.Exception);
        LogErrorToFile(t.Exception);
      }
      catch
      {
        try
        {
          MessageBox.Show("Fatal Windows Forms Error", "Fatal Windows Forms Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);
        }
        finally
        {
          Application.Exit();
        }
      }

      // Exits the program when the user clicks Abort.
      if (result == DialogResult.Abort)
        Application.Exit();
    }

    // Handle the UI exceptions by showing a dialog box, and asking the user whether
    // or not they wish to abort execution.
    // N_O_T_E: This exception cannot be kept from terminating the application - it can only 
    // log the event, and inform the user about it. 
    private static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      try
      {
        Exception ex = (Exception)e.ExceptionObject;
        string errorMsg = "An application error occurred. Please contact the adminstrator " +
            "with the following information:\n\n";

        // Since we can't prevent the app from terminating, log this to the event log.
        if (!EventLog.SourceExists("ThreadException"))
        {
          EventLog.CreateEventSource("ThreadException", "Application");
        }

        // Create an EventLog instance and assign its source.
        EventLog myLog = new EventLog() { Source = "ThreadException" };
        myLog.WriteEntry(errorMsg + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace);
        LogErrorToFile(ex);
      }
      catch (Exception exc)
      {
        try
        {
          MessageBox.Show("Fatal Non-UI Error",
              "Fatal Non-UI Error. Could not write the error to the event log. Reason: "
              + exc.Message, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }
        finally
        {
          Application.Exit();
        }
      }
    }

    private static void LogErrorToFile(Exception ex)
    {
      string filePath = $@"log\Application_Error_{CxConvert.Date}.txt";
      using (StreamWriter writer = new StreamWriter(filePath, true))
      {
        writer.WriteLine("-----------------------------------------------------------------------------");
        writer.WriteLine("Date : " + DateTime.Now.ToString());
        writer.WriteLine();

        while (ex != null)
        {
          writer.WriteLine(ex.GetType().FullName);
          writer.WriteLine("Message : " + ex.Message);
          writer.WriteLine("StackTrace : " + ex.StackTrace);

          ex = ex.InnerException;
        }
      }
    }
  }
}