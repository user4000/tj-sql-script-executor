﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Data;
using TJFramework;
using static TJFramework.TJFrameworkManager;

namespace TjSqlScriptExecutor
{
  public partial class FxSender : RadForm, IEventStartWork, IEventEndWork
  {
    public string FileSearchPattern { get; set; } = "*.sql";
    public FxSender()
    {
      InitializeComponent();
    }

    public void EventEndWork()
    {

    }

    public void EventStartWork()
    {
      SetProperties();
      SetEvents();
    }

    public void SetProperties()
    {
      LxFiles.ItemHeight = 25;
      RibbonBarMain.CloseButton = false;
      RibbonBarMain.MaximizeButton = false;
      RibbonBarMain.MinimizeButton = false;
      RibbonBarMain.ExpandButton.Visibility = ElementVisibility.Collapsed;
      RibbonBarMain.ExitButton.Visibility = ElementVisibility.Collapsed;
      RibbonBarMain.LayoutMode = RibbonLayout.Simplified;
      TxFolder.Text = Program.USettings.Folder;
      if (Directory.Exists(TxFolder.Text)) EventFolderChanged(this, new EventArgs());
    }

    public void SetEvents()
    {
      BxOpenFolder.Click += EventOpenFolder;
      TxFolder.TextChanged += EventFolderChanged;
      BxSelectFile.Click += EventReadTextFromFile;
      BxSendScript.Click += EventSendScript;
      BxClearText.Click += EventClearText;
    }

    private void EventClearText(object sender, EventArgs e)
    {
      TxEditor.Text = string.Empty;
    }

    public void EventSendScript(object sender, EventArgs e)
    {
      Ms.Message("test", TxEditor.Text).CloseOnClick().Wire(TxFolder).Debug();
    }

    public void EventReadTextFromFile(object sender, EventArgs e)
    {
      string item = LxFiles.SelectedItem?.Text ?? string.Empty;

      if(string.IsNullOrWhiteSpace(item))
      {
        TxEditor.Text = "ERROR ! You did not select any file.";
        return;
      }

      string FileName = Path.Combine(TxFolder.Text, item);
      if (!File.Exists(FileName))
      {
        TxEditor.Text = "ERROR ! This file does not exist.";
        return;
      }

      if (FileAnalyzer.IsBinary(FileName))
      {
        TxEditor.Text = "ERROR ! This file does not contain SQL commands.";
        return;
      }

      long length = new System.IO.FileInfo(FileName).Length;

      if (length > 1000000)
      {
        Ms.Message("Error", "File size is too big").Wire(BxSelectFile).CloseOnClick().Warning();
        return;
      }

      Encoding encoding = Encoding.GetEncoding("Windows-1251");

      FileStream fileStream = new FileStream(FileName, FileMode.Open, FileAccess.Read);

      /*if (fileStream.Length > 1000000)
      {
        fileStream.Dispose();
        Ms.Message("Error", "File size is too big").Wire(BxSelectFile).CloseOnClick().Warning();
        return;
      }*/

      /*
      TextReader reader = new StreamReader(fileStream, encoding);
      TxEditor.Text = reader.ReadToEnd();
      reader.Close();
      */

      TxEditor.Text = File.ReadAllText(FileName, encoding);
    }

    public void EventFolderChanged(object sender, EventArgs e)
    {
      //string[] files = Directory.GetFiles(TxFolder.Text);
      var filenames = from fullFilename
                in Directory.EnumerateFiles(TxFolder.Text, FileSearchPattern)
                      select Path.GetFileName(fullFilename);

      LxFiles.DataSource = filenames;
      LxFiles.SelectedIndex = -1;
    }

    public async void EventOpenFolder(object sender, EventArgs e)
    {
      FolderBrowserDialog dialog = new FolderBrowserDialog();
      if (string.IsNullOrWhiteSpace(TxFolder.Text))
        dialog.RootFolder = Environment.SpecialFolder.MyComputer;
      else
        dialog.SelectedPath = TxFolder.Text;

      BxOpenFolder.Visibility = ElementVisibility.Collapsed;
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        TxFolder.Text = dialog.SelectedPath;
        Program.USettings.Folder = TxFolder.Text;
      }
      await Task.Delay(200);
      BxOpenFolder.Visibility = ElementVisibility.Visible;
    }
  }
}
