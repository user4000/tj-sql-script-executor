﻿namespace TjSqlScriptExecutor
{
    partial class FxSender
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FxSender));
      this.PnTop = new Telerik.WinControls.UI.RadPanel();
      this.BxClearText = new Telerik.WinControls.UI.RadButton();
      this.BxSendScript = new Telerik.WinControls.UI.RadButton();
      this.BxSelectFile = new Telerik.WinControls.UI.RadButton();
      this.TxFolder = new Telerik.WinControls.UI.RadButtonTextBox();
      this.BxOpenFolder = new Telerik.WinControls.UI.RadImageButtonElement();
      this.PnFiles = new Telerik.WinControls.UI.RadPanel();
      this.LxFiles = new Telerik.WinControls.UI.RadListControl();
      this.PnText = new Telerik.WinControls.UI.RadPanel();
      this.TxEditor = new Telerik.WinControls.UI.RadRichTextEditor();
      this.RibbonBarMain = new Telerik.WinControls.UI.RichTextEditorRibbonBar();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.object_0d6c1184_c4da_4196_9d19_6317d0cc1c68 = new Telerik.WinControls.RootRadElement();
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).BeginInit();
      this.PnTop.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.BxClearText)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxSendScript)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxSelectFile)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxFolder)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnFiles)).BeginInit();
      this.PnFiles.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.LxFiles)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnText)).BeginInit();
      this.PnText.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxEditor)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.RibbonBarMain)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PnTop
      // 
      this.PnTop.Controls.Add(this.BxClearText);
      this.PnTop.Controls.Add(this.BxSendScript);
      this.PnTop.Controls.Add(this.BxSelectFile);
      this.PnTop.Controls.Add(this.TxFolder);
      this.PnTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnTop.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnTop.Location = new System.Drawing.Point(0, 0);
      this.PnTop.Name = "PnTop";
      this.PnTop.Size = new System.Drawing.Size(1236, 78);
      this.PnTop.TabIndex = 0;
      // 
      // BxClearText
      // 
      this.BxClearText.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxClearText.Location = new System.Drawing.Point(951, 25);
      this.BxClearText.Name = "BxClearText";
      this.BxClearText.Size = new System.Drawing.Size(160, 26);
      this.BxClearText.TabIndex = 1;
      this.BxClearText.Text = "Clear text";
      // 
      // BxSendScript
      // 
      this.BxSendScript.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxSendScript.Location = new System.Drawing.Point(730, 25);
      this.BxSendScript.Name = "BxSendScript";
      this.BxSendScript.Size = new System.Drawing.Size(160, 26);
      this.BxSendScript.TabIndex = 1;
      this.BxSendScript.Text = "Send script";
      // 
      // BxSelectFile
      // 
      this.BxSelectFile.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxSelectFile.Location = new System.Drawing.Point(501, 25);
      this.BxSelectFile.Name = "BxSelectFile";
      this.BxSelectFile.Size = new System.Drawing.Size(160, 26);
      this.BxSelectFile.TabIndex = 1;
      this.BxSelectFile.Text = "Read selected file";
      // 
      // TxFolder
      // 
      this.TxFolder.AutoSize = false;
      this.TxFolder.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxFolder.Location = new System.Drawing.Point(27, 25);
      this.TxFolder.Name = "TxFolder";
      this.TxFolder.ReadOnly = true;
      this.TxFolder.RightButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.BxOpenFolder});
      this.TxFolder.Size = new System.Drawing.Size(405, 26);
      this.TxFolder.TabIndex = 0;
      ((Telerik.WinControls.Primitives.BorderPrimitive)(this.TxFolder.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Visible;
      // 
      // BxOpenFolder
      // 
      this.BxOpenFolder.Image = ((System.Drawing.Image)(resources.GetObject("BxOpenFolder.Image")));
      this.BxOpenFolder.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.BxOpenFolder.Name = "BxOpenFolder";
      this.BxOpenFolder.ShowBorder = false;
      this.BxOpenFolder.Text = ". . .";
      ((Telerik.WinControls.Primitives.BorderPrimitive)(this.BxOpenFolder.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // PnFiles
      // 
      this.PnFiles.Controls.Add(this.LxFiles);
      this.PnFiles.Dock = System.Windows.Forms.DockStyle.Left;
      this.PnFiles.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnFiles.Location = new System.Drawing.Point(0, 78);
      this.PnFiles.Name = "PnFiles";
      this.PnFiles.Size = new System.Drawing.Size(391, 751);
      this.PnFiles.TabIndex = 0;
      // 
      // LxFiles
      // 
      this.LxFiles.Dock = System.Windows.Forms.DockStyle.Fill;
      this.LxFiles.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LxFiles.Location = new System.Drawing.Point(0, 0);
      this.LxFiles.Name = "LxFiles";
      this.LxFiles.Size = new System.Drawing.Size(391, 751);
      this.LxFiles.TabIndex = 0;
      // 
      // PnText
      // 
      this.PnText.Controls.Add(this.TxEditor);
      this.PnText.Controls.Add(this.RibbonBarMain);
      this.PnText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PnText.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnText.Location = new System.Drawing.Point(400, 78);
      this.PnText.Name = "PnText";
      this.PnText.Size = new System.Drawing.Size(836, 751);
      this.PnText.TabIndex = 0;
      // 
      // TxEditor
      // 
      this.TxEditor.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(189)))), ((int)(((byte)(232)))));
      this.TxEditor.Dock = System.Windows.Forms.DockStyle.Fill;
      this.TxEditor.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxEditor.Location = new System.Drawing.Point(0, 175);
      this.TxEditor.Name = "TxEditor";
      this.TxEditor.SelectionFill = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(78)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
      this.TxEditor.Size = new System.Drawing.Size(836, 576);
      this.TxEditor.TabIndex = 0;
      // 
      // RibbonBarMain
      // 
      this.RibbonBarMain.ApplicationMenuStyle = Telerik.WinControls.UI.ApplicationMenuStyle.BackstageView;
      this.RibbonBarMain.AssociatedRichTextEditor = this.TxEditor;
      this.RibbonBarMain.BuiltInStylesVersion = Telerik.WinForms.Documents.Model.Styles.BuiltInStylesVersion.Office2013;
      this.RibbonBarMain.EnableKeyMap = false;
      // 
      // 
      // 
      this.RibbonBarMain.ExitButton.Text = "Exit";
      this.RibbonBarMain.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.RibbonBarMain.LocalizationSettings.LayoutModeText = "Simplified Layout";
      this.RibbonBarMain.Location = new System.Drawing.Point(0, 0);
      this.RibbonBarMain.Name = "RibbonBarMain";
      // 
      // 
      // 
      this.RibbonBarMain.OptionsButton.Text = "Options";
      this.RibbonBarMain.ShowLayoutModeButton = true;
      this.RibbonBarMain.Size = new System.Drawing.Size(836, 175);
      this.RibbonBarMain.TabIndex = 1;
      this.RibbonBarMain.TabStop = false;
      this.RibbonBarMain.Text = "Script";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(1))).Text = "Insert";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(1))).Name = "tabInsert";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(2))).Text = "Page Layout";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(2))).Name = "tabPageLayout";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(3))).Text = "References";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(3))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(3))).Name = "tabReferences";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(4))).Text = "Mailings";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(4))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(4))).Name = "tabMailings";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(5))).Text = "Review";
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(5))).Enabled = true;
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(5))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      ((Telerik.WinControls.UI.RichTextEditorRibbonUI.RichTextEditorRibbonTab)(this.RibbonBarMain.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0).GetChildAt(5))).Name = "tabReview";
      // 
      // splitter1
      // 
      this.splitter1.Location = new System.Drawing.Point(391, 78);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(9, 751);
      this.splitter1.TabIndex = 1;
      this.splitter1.TabStop = false;
      // 
      // object_0d6c1184_c4da_4196_9d19_6317d0cc1c68
      // 
      this.object_0d6c1184_c4da_4196_9d19_6317d0cc1c68.Name = "object_0d6c1184_c4da_4196_9d19_6317d0cc1c68";
      this.object_0d6c1184_c4da_4196_9d19_6317d0cc1c68.StretchHorizontally = true;
      this.object_0d6c1184_c4da_4196_9d19_6317d0cc1c68.StretchVertically = true;
      // 
      // FxSender
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1236, 829);
      this.Controls.Add(this.PnText);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.PnFiles);
      this.Controls.Add(this.PnTop);
      this.Name = "FxSender";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "FxSender";
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).EndInit();
      this.PnTop.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.BxClearText)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxSendScript)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxSelectFile)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxFolder)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnFiles)).EndInit();
      this.PnFiles.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.LxFiles)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnText)).EndInit();
      this.PnText.ResumeLayout(false);
      this.PnText.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxEditor)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.RibbonBarMain)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPanel PnTop;
    public Telerik.WinControls.UI.RadPanel PnFiles;
    public Telerik.WinControls.UI.RadPanel PnText;
    private System.Windows.Forms.Splitter splitter1;
    private Telerik.WinControls.UI.RadButtonTextBox TxFolder;
    public Telerik.WinControls.UI.RadImageButtonElement BxOpenFolder;
    private Telerik.WinControls.RootRadElement object_0d6c1184_c4da_4196_9d19_6317d0cc1c68;
    private Telerik.WinControls.UI.RadListControl LxFiles;
    private Telerik.WinControls.UI.RadButton BxClearText;
    private Telerik.WinControls.UI.RadButton BxSendScript;
    public Telerik.WinControls.UI.RadButton BxSelectFile;
    public Telerik.WinControls.UI.RichTextEditorRibbonBar RibbonBarMain;
    public Telerik.WinControls.UI.RadRichTextEditor TxEditor;
  }
}
