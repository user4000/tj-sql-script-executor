﻿using System;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading.Tasks;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using ProjectStandard;
using System.Collections.Generic;
using TJFramework;
using ModelStandard;
using static TJFramework.Logger.Manager;
using static TJFramework.TJFrameworkManager;
using static TjSqlScriptExecutor.Program;

namespace TjSqlScriptExecutor
{
  public class CxManager : IOutputMessage
  {
    internal CxConnectionManager CnManager { get; set; } = null;

    internal CxLoginManager LoginManager { get; set; } = null;

    internal CxTextWriter TxtWriter { get; private set; } = null;

    internal Timer TimerHideMainForm { get; } = new Timer();

    public RadFormLogin FmLogin { get; private set; }

    public int ConnectionCount { get; private set; } = 0;

    public string GetCurrentDatabase { get => USettings.Database; }

    public string GetCurrentServer { get => USettings.Server; }

    public string GetCurrentLogin { get => USettings.Login; }

    public void OutputMessage(string message, string header = "")
    {

    }

    public async Task EventConnectedFirstTime()
    {
      await Task.Delay(100);
      //Ms.Message("test", "EventConnectedFirstTime").CloseOnClick().Pos(MsgPos.ScreenCenter).Debug();
    }

    public async Task EventConnectedToServer(string login, string password)
    {
      await Task.Delay(100);
      USettings.Login = login;
      USettings.HiddenTimeOfLastSuccessLogin = DateTime.Now;
      if (ConnectionCount++ == 0)
      {
        await EventConnectedFirstTime();
      }
    }

    public async Task EventDisconnectedFromServer()
    {
      await Task.Delay(100);
    }


    internal void RedirectConsoleOutput()
    {
      TxtWriter = new CxTextWriter(this); Console.SetOut(TxtWriter);
    }


    internal void EventBeforeAnyFormStart()
    {
      
    }

    internal async Task EventStartWork() // -------------------------------------------------------------------------------------------------------------- //
    {
      await Task.Delay(100);
      FmLogin = Pages.GetRadForm<RadFormLogin>();
      LoginManager.Configure(FmLogin);
    }

    public async Task EventEndWork()
    {
      await LoginManager.Disconnect();
    }

    public async Task<ReturnCode> ChangePassword(string Login, string OldPassword, string NewPassword)
    {
      await Task.Delay(100);
      return ReturnCodeFactory.ErrorCode("test");
    }

    public void Debug(string message, string header = "TEST") // TODO: Remove all calls for release version //
    {
      Ms.Message(header, message).Pos(MsgPos.TopRight).NoAlert().Debug();
      MainForm.Text = header + " " + message;
    }

    public void LogObject(object message, string FileName = "TEST") => LogToFile(message.GetType().Name + " = " + CxConvert.ObjectToJson(message), FileName);

    public void LogToFile(string message, string FileName = "TEST")
    {
      string file = @"d:\temp\1______" + FileName + ".txt";  //+ Number.ToString() + "___" + CxConvert.Time.Replace(":", "_") + "_" + CxSecurityStandard.GenerateRandomId(5) + ".txt";
      File.AppendAllText(file, message + "\r\n\r\n");
    }

    public void EventSelectedPageChanged(string obj)
    {

    }
  }
}

