﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace TjSqlScriptExecutor
{
  public class CxConnectionManager
  {
    private string MsSqlServer { get; set; } = string.Empty;
    private string Database { get; set; } = string.Empty;
    private string Login { get; set; } = string.Empty;
    private string Password { get; set; } = string.Empty;
    private string ConnectionString { get; set; } = string.Empty;

    public CxConnectionManager()
    {

    }

    public SqlConnection GetNewConnection() => new SqlConnection(ConnectionString);

    public string GetConnectionString()
    {
      return GetConnectionString(MsSqlServer, Database, Login, Password);
    }

    public string GetConnectionString(string server, string database, string login, string password)
    {
      SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder($"Data Source={server};Initial Catalog={database}");
      builder.UserID = login;
      builder.Password = password;
      return builder.ConnectionString;
    }

    public void InitConnectionString(string server, string database, string login, string password)
    {
      ConnectionString = GetConnectionString(server, database, login, password);
      MsSqlServer = server; Database = database; Login = login; Password = password;
    }

    public void InitConnectionString() => InitConnectionString(MsSqlServer, Database, Login, Password);

    public async Task<string> CheckServerConnection(string connectionString)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        try
        {
          await connection.OpenAsync();
          return string.Empty;
        }
        catch (Exception ex)
        {
          return ex.Message;
        }
      }
    }

    public async Task<string> CheckServerConnection() => await CheckServerConnection(ConnectionString);

    public void Disconnect()
    {
      Password = string.Empty;
      ConnectionString = string.Empty;
    }

    public string ChangePassword(string OldPassword, string NewPassword)
    {
      string result = string.Empty;
      string currentPassword = Password;

      InitConnectionString(MsSqlServer, Database, Login, OldPassword);
      try
      {
        SqlConnection.ChangePassword(ConnectionString, NewPassword);
      }
      catch (Exception ex)
      {
        result = ex.Message;
        InitConnectionString(MsSqlServer, Database, Login, currentPassword);
      }
      return result;
    }
  }
}

