﻿using System;
using System.Threading.Tasks;
using ProjectStandard;
using Telerik.WinControls;
using TJFramework;
using static TJFramework.TJFrameworkManager;

namespace TjSqlScriptExecutor
{
  public class CxLoginManager
  {
    private CxManager Manager { get; set; } = null;

    private RadFormLogin FormLogin { get; set; } = null;

    public bool FlagConnected { get; private set; } = false;

    public byte MinimumPasswordLength { get; } = 5;

    public CxLoginManager(CxManager manager)
    {
      Manager = manager; // Ms.Message("test", "test").CloseOnClick().Pos(MsgPos.ScreenCenter).Debug(); 
    }

    public void Configure(RadFormLogin form)
    {
      FormLogin = form;
      FormLogin.PageViewLogin.SelectedPage = FormLogin.PageLogin;
      FormLogin.TxPassword.Focus();

      FormLogin.PageHint.Item.Visibility = ElementVisibility.Collapsed;
      FormLogin.TxLogin.Text = Manager.GetCurrentLogin;
      SetFormControls();
      FormLogin.BtnConnect.Click += EventConnectButton;
      FormLogin.BtnChangePassword.Click += EventChangePasswordButton;
      FormLogin.PicHint.Click += EventPictureHintClick;
    }

    public void SetFormControls()
    {
      if (FlagConnected)
      {
        FormLogin.BtnConnect.Text = FormLogin.LbDisconnect.Text;
        FormLogin.PicConnection.Image = FormLogin.PictureOn.Image;
        FormLogin.TxPassword.Clear();
        FormLogin.PagePassword.Enabled = true;
        //FormLogin.PagePassword.Item.Visibility = ElementVisibility.Visible;
      }
      else
      {
        FormLogin.BtnConnect.Text = FormLogin.LbConnect.Text;
        FormLogin.PicConnection.Image = FormLogin.PictureOff.Image;
        FormLogin.PagePassword.Enabled = false;
        //FormLogin.PagePassword.Item.Visibility = ElementVisibility.Collapsed;
      }
      FormLogin.TxLogin.Enabled = !FlagConnected;
      FormLogin.TxPassword.Enabled = !FlagConnected;
    }

    private void EventPictureHintClick(object sender, EventArgs e)
    {
      FormLogin.PageViewLogin.SelectedPage = FormLogin.PageHint;
    }


    private bool CheckNewPassword()
    {
      if (FormLogin.TxtNew1.Text.Length < MinimumPasswordLength)
      {
        Ms.Message("You password is too short",  $"Password must be at least {MinimumPasswordLength} characters").CloseOnClick().Wire(FormLogin.TxtNew1).Fail();
        return false;
      }

      if (FormLogin.TxtNew1.Text != FormLogin.TxtNew2.Text)
      {
        Ms.Message("Password mismatch", "You have to enter a new password twice").CloseOnClick().Wire(FormLogin.TxtNew2).Fail();
        return false;
      }

      return true;
    }

    private async void EventChangePasswordButton(object sender, EventArgs e)
    {
      if (!CheckNewPassword()) return;
      string NewPassword = FormLogin.TxtNew1.Text;
      string OldPassword = FormLogin.TxtOld.Text;

      string error = Manager.CnManager.ChangePassword(OldPassword, NewPassword);

      if (string.IsNullOrWhiteSpace(error))
      {
        await Disconnect();

        FormLogin.TxtOld.Clear();
        FormLogin.TxtNew1.Clear();
        FormLogin.TxtNew2.Clear();

        FormLogin.PageViewLogin.SelectedPage = FormLogin.PageLogin;
        Ms.Message("Your password has been changed", "Reconnect to the database using your new password").Wire(FormLogin.PictureUser).CloseOnClick().Delay(3).Ok();
        SetFormControls();
      }
      else
      {
        Ms.Message("Your password was not changed", error).Wire(FormLogin.TxtNew1).CloseOnClick().Delay(10).Warning();
      }
    }

    public async Task Connect()
    {
      string Login = FormLogin.TxLogin.Text;
      string Password = FormLogin.TxPassword.Text;
      Manager.CnManager.InitConnectionString(Manager.GetCurrentServer, Manager.GetCurrentDatabase, Login, Password);
      string error = await Manager.CnManager.CheckServerConnection();
      FlagConnected = string.IsNullOrWhiteSpace(error);

      if (FlagConnected)
      {
        await Manager.EventConnectedToServer(Login, Password);
      }
      else
      {
        Ms.Message("Connection is not established", error).Wire(FormLogin.PicConnection).CloseOnClick().Delay(10).Warning();
        FormLogin.TxPassword.Focus();
        FormLogin.TxPassword.SelectAll();
      }
    }

    public async Task Disconnect()
    {
      Manager.CnManager.Disconnect();
      FlagConnected = false;
      await Manager.EventDisconnectedFromServer();
    }


    private async void EventConnectButton(object sender, EventArgs e)
    {
      FormLogin.BtnConnect.Enabled = false;

      if (FlagConnected)
      {
        await Disconnect();
      }
      else
      {
        await Connect();
      }

      SetFormControls();
      FormLogin.BtnConnect.Enabled = true;
    }
  }
}

