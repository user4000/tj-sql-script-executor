﻿namespace TjSqlScriptExecutor
{
  internal class CxManagerFactory
  {
    private static CxManager Manager = null;

    public static CxManager Create()
    {
      if (Manager == null)
      {
        Manager = new CxManager();
        InitMembersOfManager(Manager);
      }
      return Manager;
    }

    private static void InitMembersOfManager(CxManager manager)
    {
      manager.CnManager = new CxConnectionManager();
      manager.LoginManager = new CxLoginManager(manager);
    }
  }
}
