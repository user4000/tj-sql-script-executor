﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TjSqlScriptExecutor
{
  public class FileAnalyzer
  {
    public static void TestBinaryFile(string folderPath)
    {
      List<string> output = new List<string>();
      foreach (string filePath in GetFiles(folderPath, true))
      {
        output.Add(IsBinary(filePath).ToString() + "  ----  " + filePath);
      }
      Clipboard.SetText(string.Join("\n", output), TextDataFormat.Text);
    }

    public static List<string> GetFiles(string path, bool recursive = false)
    {
      return Directory.Exists(path) ?
          Directory.GetFiles(path, "*.*",
          recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).ToList() :
          new List<string>();
    }

    public static bool IsBinary(string FileName)
    {
      long length = new FileInfo(FileName).Length;
      if (length == 0) return false;

      using (StreamReader stream = new StreamReader(FileName))
      {
        int ch;
        while ((ch = stream.Read()) != -1)
        {
          if (IsControlChar(ch))
          {
            return true;
          }
        }
      }
      return false;
    }

    public static bool IsControlChar(int ch)
    {
      return (ch > Chars.NUL && ch < Chars.BS) || (ch > Chars.CR && ch < Chars.SUB);
    }

    public static class Chars
    {
      public static char NUL = (char)0; // Null char
      public static char BS = (char)8; // Back Space
      public static char CR = (char)13; // Carriage Return
      public static char SUB = (char)26; // Substitute
    }
  }
}
